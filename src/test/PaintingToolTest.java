package test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import vecFrame.PaintingTool;
import vecFrame.PaintingTool.*;


import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import static org.junit.jupiter.api.Assertions.*;

class PaintingToolTest {
    // setup input
    private PaintingTool paintingTool;

    @BeforeEach
    void reset() {
        paintingTool = new PaintingTool();
    }

    @Test
    void constructor() {
        assertEquals(DrawMode.PLOT, paintingTool.getDrawMode());
    }

    @Test
    void setDrawModeEventHandler() {
        final String[] event = new String[1];
        paintingTool.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                event[0] = evt.getPropertyName();
            }
        });
        paintingTool.setDrawMode(DrawMode.POLYGON);
        assertEquals("tool changed", event[0]);
    }

    @Test
    void setDrawModeEventHandlerNoChange() {
        final String[] event = new String[1];
        paintingTool.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                event[0] = evt.getPropertyName();
            }
        });
        paintingTool.setDrawMode(DrawMode.PLOT);
        assertEquals(null, event[0]);
    }


    @Test
    void setDrawModeEventHandler2() {
        DrawMode expectedOldMode = DrawMode.PLOT;
        DrawMode expectedNewMode = DrawMode.POLYGON;
        final DrawMode[] oldMode = new DrawMode[1];
        final DrawMode[] newMode = new DrawMode[1];

        paintingTool.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                oldMode[0] = (DrawMode) evt.getOldValue();
                newMode[0] = (DrawMode) evt.getNewValue();
            }
        });
        paintingTool.setDrawMode(expectedNewMode);
        assertEquals(expectedOldMode, oldMode[0]);
        assertEquals(expectedNewMode, newMode[0]);
    }

    @Test
    void setDrawMode() {
        DrawMode expected = DrawMode.RECTANGLE;

        paintingTool.setDrawMode(DrawMode.RECTANGLE);
        assertEquals(expected, paintingTool.getDrawMode());
    }
}