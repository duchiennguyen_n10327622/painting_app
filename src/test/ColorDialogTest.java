package test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import property.ColorDialog;

import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import static org.junit.jupiter.api.Assertions.*;

class ColorDialogTest {
    // setup input
    ColorDialog colorDialog;

    @BeforeEach
    void reset() {
        colorDialog = new ColorDialog();
    }

    @Test
    void constructorTest() {
        assertEquals(Color.BLACK, colorDialog.getPenColor());
        assertEquals(Color.WHITE, colorDialog.getFillColor());
        assertEquals(true, colorDialog.getFillIsOn());
    }

    @Test
    void setPenColor() {
        colorDialog.setPenColor(Color.WHITE);
        assertEquals(Color.WHITE, colorDialog.getPenColor());
    }

    @Test
    void setPenColorPropertyListener() {
        final String[] event = {new String()};

        colorDialog.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                event[0] = evt.getPropertyName();
            }
        });

        colorDialog.setPenColor(Color.WHITE);
        assertEquals("Pen color changed", event[0]);
    }

    @Test
    void setPenColorPropertyListenerNoChange() {
        final String[] event = {new String()};

        colorDialog.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                event[0] = evt.getPropertyName();
            }
        });

        colorDialog.setPenColor(Color.BLACK);
        assertEquals("", event[0]);
    }

    @Test
    void setPenColorPropertyListenerValue() {
        Color oldColorExpected = Color.BLACK;
        Color newColorExpected = Color.WHITE;
        final Color[] oldColor = new Color[1];
        final Color[] newColor = new Color[1];

        colorDialog.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                oldColor[0] = (Color)evt.getOldValue();
                newColor[0] = (Color)evt.getNewValue();
            }
        });

        colorDialog.setPenColor(newColorExpected);
        assertEquals(oldColorExpected, oldColor[0]);
        assertEquals(newColorExpected, newColor[0]);

    }

    @Test
    void setFillColor() {
        colorDialog.setFillColor(Color.BLUE);
        assertEquals(Color.BLUE, colorDialog.getFillColor());
    }

    @Test
    void setFillColorPropertyListener() {
        final String[] event = {new String()};

        colorDialog.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                event[0] = evt.getPropertyName();
            }
        });

        colorDialog.setFillColor(Color.BLACK);
        assertEquals("Fill color changed", event[0]);
    }

    @Test
    void setFillColorPropertyListenerNoChange() {
        final String[] event = {new String()};

        colorDialog.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                event[0] = evt.getPropertyName();
            }
        });

        colorDialog.setFillColor(Color.WHITE);
        assertEquals("", event[0]);
    }

    @Test
    void setFillColorPropertyListenerValue() {
        Color oldColorExpected = Color.WHITE;
        Color newColorExpected = Color.BLACK;
        final Color[] oldColor = new Color[1];
        final Color[] newColor = new Color[1];

        colorDialog.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                oldColor[0] = (Color)evt.getOldValue();
                newColor[0] = (Color)evt.getNewValue();
            }
        });

        colorDialog.setFillColor(newColorExpected);
        assertEquals(oldColorExpected, oldColor[0]);
        assertEquals(newColorExpected, newColor[0]);

    }

    @Test
    void setFillIsOn() {
        colorDialog.setFillIsOn(false);
        assertEquals(false, colorDialog.getFillIsOn());
    }

    @Test
    void setFillIsOnPropertyListener() {
        final String[] event = {new String()};

        colorDialog.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                event[0] = evt.getPropertyName();
            }
        });

        colorDialog.setFillIsOn(false);
        assertEquals("Fillstate changed", event[0]);
    }

    @Test
    void setFillIsOnPropertyListenerNoChange() {
        final String[] event = {new String()};

        colorDialog.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                event[0] = evt.getPropertyName();
            }
        });

        colorDialog.setFillIsOn(true);
        assertEquals("", event[0]);
    }

    @Test
    void setFillIsOnPropertyListenerValue() {
        Boolean expectedOldValue = true;
        Boolean expectedNewValue = false;
        final Boolean[] oldValue = new Boolean[1];
        final Boolean[] newValue = new Boolean[1];

        colorDialog.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                oldValue[0] = (Boolean)evt.getOldValue();
                newValue[0] = (Boolean)evt.getNewValue();
            }
        });

        colorDialog.setFillIsOn(false);
        assertEquals(expectedOldValue, oldValue[0]);
        assertEquals(expectedNewValue, newValue[0]);
    }
}