package test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import property.ColorDialog;
import vecFrame.Menu;
import vecFrame.PaintingPad;
import vecFrame.PaintingTool;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class PaintingPadTest {
    //set up input
    private PaintingPad paintingPad;
    private ColorDialog colorDialog;

    @BeforeEach
    void reset() {
        colorDialog = new ColorDialog();
        paintingPad = new PaintingPad(700, 700);
        paintingPad.setColorDialog(colorDialog);
    }

    @Test
    void constructorTest() {
        assertEquals(700, paintingPad.getWidth());
        assertEquals(700, paintingPad.getHeight());
        assertEquals("",paintingPad.getVecFile());
        assertEquals(Color.BLACK, colorDialog.getPenColor());
        assertEquals(Color.WHITE, colorDialog.getFillColor());
    }

    @Test
    void getColor() {
        assertEquals(Color.WHITE, paintingPad.getColor("#FFFFFF"));
        assertEquals(Color.BLACK, paintingPad.getColor("#000000"));
    }

    @Test
    void drawString() {
        String stringToDraw = "LINE 0.5 0.5 1.0 1.0";
        paintingPad.drawString(stringToDraw);
        assertEquals(stringToDraw + "\n", paintingPad.getVecFile());
    }

    @Test
    void redo() {
        String[] stringToDraw = new String[]{"LINE 0.0 0.0 1.0 1.0",
                "RECTANGLE 0.5 0.5 0.7 0.7",
                "POLYGON 0.0 0.0 0.3 0.5 0.7 0.8"
        };
        String expected = stringToDraw[0] + "\n" + stringToDraw[1] +"\n";

        paintingPad.drawString(stringToDraw[0]);
        paintingPad.drawString(stringToDraw[1]);
        paintingPad.drawString(stringToDraw[2]);
        paintingPad.undo();
        paintingPad.undo();
        paintingPad.redo();
        assertEquals(expected, paintingPad.getVecFile());
    }

    @Test
    void redoWithEmptyRedoState() {
        String[] stringToDraw = new String[]{"LINE 0.0 0.0 1.0 1.0",
                "RECTANGLE 0.5 0.5 0.7 0.7",
                "POLYGON 0.0 0.0 0.3 0.5 0.7 0.8"
        };
        String expected = stringToDraw[0] + "\n" + stringToDraw[1] +"\n" + stringToDraw[2] + "\n";

        paintingPad.drawString(stringToDraw[0]);
        paintingPad.drawString(stringToDraw[1]);
        paintingPad.drawString(stringToDraw[2]);
        paintingPad.redo();
        assertEquals(expected, paintingPad.getVecFile());
    }

    @Test
    void undo() {
        String[] stringToDraw = new String[]{"LINE 0.0 0.0 1.0 1.0",
                "RECTANGLE 0.5 0.5 0.7 0.7",
                "POLYGON 0.0 0.0 0.3 0.5 0.7 0.8"
        };
        String expected = stringToDraw[0] + "\n" + stringToDraw[1] +"\n";

        paintingPad.drawString(stringToDraw[0]);
        paintingPad.drawString(stringToDraw[1]);
        paintingPad.drawString(stringToDraw[2]);
        paintingPad.undo();
        assertEquals(expected, paintingPad.getVecFile());
    }

    @Test
    void undoWithEmptyVec() {
        paintingPad.undo();
        assertEquals("", paintingPad.getVecFile());
    }

    @Test
    void load() throws IOException {
        String[] stringToDraw = new String[]{"LINE 0.0 0.0 1.0 1.0",
                "RECTANGLE 0.5 0.5 0.7 0.7",
                "POLYGON 0.0 0.0 0.3 0.5 0.7 0.8"
        };
        String expected = stringToDraw[0] + "\n" + stringToDraw[1] +"\n";

        File file = new File("src/test/testFile/vecFile.vec");
        file.createNewFile();

        FileOutputStream fout = new FileOutputStream(file);
        byte expectedByte[] = expected.getBytes();
        fout.write(expectedByte);
        fout.close();

        paintingPad.load(file);
        assertEquals(expected, paintingPad.getVecFile());
    }
}