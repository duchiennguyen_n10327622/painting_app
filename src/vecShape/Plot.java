package vecShape;

import java.awt.*;

public class Plot extends Shape implements DrawType {
    /**
     * draw the plot
     * @param g2d graphics2D
     */
    @Override
    public void draw(Graphics2D g2d) {
        g2d.setColor(penColor);
        g2d.drawLine(start.x, start.y, end.x, end.y);
    }
}
