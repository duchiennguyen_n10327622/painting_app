package vecShape;

import java.awt.*;

public class Picker extends Shape implements DrawType{
    private Color color;

    /**
     * color picker draw nohthing
     * @param g2d graphics2D
     */
    @Override
    public void draw(Graphics2D g2d) {

    }

    /**
     * set color for color picker
     * @param color color
     */
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     * return the gotten color
     * @return color
     */
    public Color getColor() {
        return  this.color;
    }
}
