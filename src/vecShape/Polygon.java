package vecShape;

import java.awt.*;

public class Polygon extends Shape implements DrawType{

    public Color fillColor;
    private int[] xPoints;
    private int[] yPoints;

    /**
     * draw polygon
     * @param g2d graphics2D
     */
    @Override
    public void draw(Graphics2D g2d) {
        xPoints = points.stream().mapToInt(p -> p.x).toArray();
        yPoints = points.stream().mapToInt(p -> p.y).toArray();

        if (fillIsOn) {
            g2d.setColor(fillColor);
            g2d.fillPolygon(xPoints, yPoints, points.size());
        }

        g2d.setColor(penColor);
        g2d.drawPolygon(xPoints, yPoints, points.size());
    }

    /**
     * set fill color for polygon
     * @param fillColor fill color
     */
    public void setFillColor(Color fillColor) {
        this.fillColor = fillColor;
    }
}
