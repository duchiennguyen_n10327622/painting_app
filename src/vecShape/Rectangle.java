package vecShape;

import java.awt.*;

public class Rectangle extends Shape implements DrawType{
    private Color fillColor;

    /**
     * draw rectangle
     * @param g2d graphics2D
     */
    @Override
    public void draw(Graphics2D g2d) {
        if (fillIsOn) {
            g2d.setColor(fillColor);
            g2d.fillRect(Math.min(start.x, end.x), Math.min(start.y, end.y),
                    Math.abs(start.x - end.x), Math.abs(start.y - end.y));
        }

        g2d.setColor(penColor);
        g2d.drawRect(Math.min(start.x, end.x), Math.min(start.y, end.y),
                Math.abs(start.x - end.x), Math.abs(start.y - end.y));
    }

    /**
     * set fill color for rectangle
     * @param fillColor fill color
     */
    public void setFillColor(Color fillColor) {
        this.fillColor = fillColor;
    }
}
