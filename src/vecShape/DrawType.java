package vecShape;

import java.awt.*;

/**
 * Draw interface
 */
public interface DrawType {

    public void draw(Graphics2D g2d);
}
