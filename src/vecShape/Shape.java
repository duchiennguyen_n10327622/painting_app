package vecShape;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Super class for making shapes
 */
public class Shape implements Serializable {

    // shapes' components
    protected Color penColor;
    protected ArrayList<Point> points = new ArrayList<>();
    protected Point start, end;
    protected Boolean fillIsOn;

    /**
     * set points for shape
     * @param start start point
     * @param end end point
     */
    public void setPoint(Point start, Point end) {
        this.start = start;
        this.end = end;
    }

    /**
     * add points to shapes (used for polygon)
     * @param point point to be added to shapes
     */
    public void addPoints(Point point) {
        points.add(point);
    }

    /**
     * get ArrayList of points
     * @return array list of points
     */
    public ArrayList<Point> getPoints() {
        return points;
    }

    /**
     * set fill status for shape
     * @param fillIsOn filling's state
     */
    public void setFillIsOn(boolean fillIsOn) {
        this.fillIsOn = fillIsOn;
    }

    /**
     * set pen's color
     * @param penColor new pen color
     */
    public void setPenColor(Color penColor) {
        this.penColor = penColor;
    }

    /**
     * get current pen's color
     * @return current pencolor
     */
    public Color getPenColor() {
        return this.penColor;
    }
}
