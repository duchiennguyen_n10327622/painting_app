package vecFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * painting tool panel
 */
public class PaintingTool extends JPanel {
    private ButtonGroup btnGrpPaintingTools;
    private JToggleButton btnColorPicker, btnLine, btnPlot, btnRectangle, btnEllipse, btnPolygon;

    // create drawing modes enum
    public  static enum DrawMode {PLOT, LINE, RECTANGLE, ELLIPSE, POLYGON, PICKER}

    private DrawMode drawMode = DrawMode.PLOT;

    /**
     * create paintingtool panel
     */
    public PaintingTool() {
        initComponents();
        btnPlot.setIcon(new ImageIcon(getImageIcon("/icon/pen.png")));
        btnLine.setIcon(new ImageIcon(getImageIcon("/icon/line.png")));
        btnRectangle.setIcon(new ImageIcon(getImageIcon("/icon/rect.png")));
        btnColorPicker.setIcon(new ImageIcon(getImageIcon("/icon/picker.png")));
        btnEllipse.setIcon(new ImageIcon(getImageIcon("/icon/ellipse.png")));
        btnPolygon.setIcon(new ImageIcon(getImageIcon("/icon/polygon.png")));

    }

    /**
     * initialize components and add listener
     */
    private void initComponents() {
        btnGrpPaintingTools = new ButtonGroup();
        btnPlot = new JToggleButton("Plot");
        btnLine = new JToggleButton("Line");
        btnRectangle = new JToggleButton("Rectangle");
        btnColorPicker = new JToggleButton("Color Picker");
        btnEllipse = new JToggleButton("Ellipse");
        btnPolygon = new JToggleButton("Polygon");

        btnGrpPaintingTools.add(btnPlot);
        btnPlot.setSelected(true);
        btnPlot.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setDrawMode(DrawMode.PLOT);
            }
        });

        btnGrpPaintingTools.add(btnLine);
        btnLine.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setDrawMode(DrawMode.LINE);
            }
        });
        
        btnGrpPaintingTools.add(btnRectangle);
        btnRectangle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setDrawMode(DrawMode.RECTANGLE);
            }
        });
        
        btnGrpPaintingTools.add(btnColorPicker);
        btnColorPicker.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setDrawMode(DrawMode.PICKER);
            }
        });
        
        btnGrpPaintingTools.add(btnEllipse);
        btnEllipse.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setDrawMode(DrawMode.ELLIPSE);
            }
        });
        
        btnGrpPaintingTools.add(btnPolygon);
        btnPolygon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setDrawMode(DrawMode.POLYGON);
            }
        });
        
        // Layouts setup
        this.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        
        // configure constraints
        gbc.insets = new Insets(2, 2, 2, 2);
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        
        // adding painting tools buttons
        gbc.gridx = 0;
        gbc.gridy = 0;
        this.add(btnPlot, gbc);
        
        gbc.gridx = 1;
        this.add(btnLine, gbc);
        
        gbc.gridx = 2;
        this.add(btnRectangle, gbc);
        
        gbc.gridx = 0;
        gbc.gridy = 1;
        this.add(btnColorPicker, gbc);
        
        gbc.gridx = 1;
        this.add(btnEllipse, gbc);
        
        gbc.gridx = 2;
        this.add(btnPolygon, gbc);
    }

    /**
     * set new draw mode
     * @param newDrawMode draw mode to be set to
     */
    public void setDrawMode(DrawMode newDrawMode) {
        DrawMode oldDrawMode = this.drawMode;
        this.drawMode = newDrawMode;
        this.firePropertyChange("tool changed", oldDrawMode, newDrawMode);
    }

    /**
     * get current draw mode
     * @return draw mode
     */
    public DrawMode getDrawMode() {
        return drawMode;
    }

    /**
     * get image from a specified path
     * @param path path to get image
     * @return image
     */
    public Image getImageIcon(String path){
        Image image = Toolkit.getDefaultToolkit().getImage(getClass().getResource(path));
        return image;
    }
}
