package vecFrame;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

/**
 * menu bar
 */
public class Menu extends JMenuBar {
    private final JMenu fileMenu;
    private final JMenuItem loadFileItem;
    private final JMenuItem saveAsFileItem;
    private final JMenuItem saveFileItem;

    private String vecFile;
    private File file;
    private PaintingPad paintingPad;

    /**
     * Create menu bar
     */
    public Menu() {
        // build file menu
        fileMenu = new JMenu("File");

        // create load menu item and assign approriate listener
        loadFileItem = new JMenuItem("Load");
        loadFileItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                load();
            }
        });

        // create save as menu item and assign approriate listener
        saveAsFileItem = new JMenuItem("Save as");
        saveAsFileItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveAs();
            }
        });

        // create save menu item and assign approriate listener
        saveFileItem = new JMenuItem("Save");
        saveFileItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                save();
            }
        });

        // add menu item to the menu
        fileMenu.add(loadFileItem);
        fileMenu.add(saveAsFileItem);
        fileMenu.add(saveFileItem);

        // add menu to bar
        this.add(fileMenu);
    }

    /**
     * Open load dialog and load file when load menu clicked
     */
    private void load() {
        try
        {
            // open selected file
            JFileChooser open = new JFileChooser();
            int option = open.showOpenDialog(loadFileItem);
            file = new File(open.getSelectedFile().getPath());

            // load file into painting canvas
            paintingPad.load(file);
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
    }

    /**
     * open Save menu and save when save as menu clicked
     */
    protected void saveAs() {
        JFileChooser fileChooser = new JFileChooser();
        int retval = fileChooser.showSaveDialog(saveAsFileItem);
        if (retval == JFileChooser.APPROVE_OPTION) {
            // save file
            file = fileChooser.getSelectedFile();

            // do nothing if no file specified
            if (file == null) {
                return;
            }

            // if file name doesn't have vec extension at the end, add vec extension
            if (!file.getName().toLowerCase().endsWith(".vec")) {
                file = new File(file.getParentFile(), file.getName() + ".vec");
            }
            // get the vec file from painting canvas to write into specified file
            try {
                FileOutputStream fout = new FileOutputStream(file);
                vecFile = paintingPad.getVecFile();
                byte vecFileByte[] = vecFile.getBytes();
                fout.write(vecFileByte);
                fout.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * save to current file if exist and open save as dialog if not exist
     */
    protected void save() {
        // save to current specified file if exist and open save as if not exist
        if (file == null) {
            saveAs();
        } else {
            // save to the painting canvas' vec file
            try {
                FileOutputStream fout = new FileOutputStream(file);
                vecFile = paintingPad.getVecFile();
                byte vecFileByte[] = vecFile.getBytes();
                fout.write(vecFileByte);
                fout.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * attach painting to current menu to get vec file
     * @param paintingPad painting pad to be added
     */
    public void setPaintingPad(PaintingPad paintingPad) {
        this.paintingPad = paintingPad;
    }
}
