package vecFrame;

import property.ColorDialog;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import static javax.swing.border.BevelBorder.RAISED;

/**
 * Mainframe
 */
public class MainFrame extends JFrame implements Runnable {

    private JPanel pnlOptions, pnlmenu, pnlCanvas;
    private JButton btnUndo, btnRedo;
    private PaintingTool paintingTool;
    private ColorDialog colorDialog;
    private JPanel pnlBG = new JPanel();
    private Menu menuBar;

    private PaintingPad paintingPad = null;

    /**
     * Mainframe
     */
    public  MainFrame() {
        super("Vector Painting Tool");
    }

    /**
     * create the GUI
     */
    public void createGUI() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new GridBagLayout());
        setBackground(new java.awt.Color(233, 247, 247));

        // initialize pannels
        pnlmenu = new JPanel();
        pnlCanvas = new JPanel();
        colorDialog = new ColorDialog();

        paintingTool = new PaintingTool();

        pnlOptions = new JPanel();
        pnlOptions.setLayout(new GridBagLayout());

        // initialize buttons and assignment approriate listeners
        btnUndo = new JButton();
        btnUndo.setIcon(new ImageIcon(getImageIcon("/icon/undo.png")));
        btnUndo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                paintingPad.undo();
            }
        });

        btnRedo = new JButton();
        btnRedo.setIcon(new ImageIcon(getImageIcon("/icon/redo.png")));
        btnRedo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                paintingPad.redo();
            }
        });

        // initialize painting pad
        paintingPad = new PaintingPad(700, 700);
        pnlBG.setLayout(null);
        pnlBG.setBackground(new Color(204, 204, 255));
        pnlBG.setPreferredSize(new Dimension(paintingPad.getWidth() + 50, paintingPad.getHeight() + 70));
        pnlBG.add(paintingPad);
        paintingPad.setLocation(5, 5);
        paintingPad.setBorder(BorderFactory.createEtchedBorder(RAISED));
        paintingPad.setColorDialog(colorDialog);
        paintingPad.setPaintingTool(paintingTool);
        paintingPad.flush();

        // initialize menubar
        menuBar = new Menu();
        menuBar.setPaintingPad(paintingPad);
        this.setJMenuBar(menuBar);

        // undo ctrl + z
        KeyStroke ctrlZ = KeyStroke.getKeyStroke(KeyEvent.VK_Z,InputEvent.CTRL_MASK);
        InputMap undoMap = btnUndo.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        undoMap.put(ctrlZ, "Undo");
        btnUndo.getActionMap().put("Undo", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                paintingPad.undo();
            }
        });

        // redo ctrl + y
        KeyStroke ctrlY = KeyStroke.getKeyStroke(KeyEvent.VK_Y,InputEvent.CTRL_MASK);
        InputMap redoMap = btnRedo.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        redoMap.put(ctrlY, "Redo");
        btnRedo.getActionMap().put("Redo", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                paintingPad.redo();
            }
        });

        // setup layouts
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(2, 2, 2, 2);
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;

        // undo button
        pnlOptions.add(btnUndo, gbc);


        // redo button
        gbc.gridx = 1;
        pnlOptions.add(btnRedo, gbc);


        // panel layouts
        gbc = new GridBagConstraints();

        // undo and redo panels
        gbc.gridx = 0;
        gbc.gridy = 0;
        this.getContentPane().add(pnlOptions);

        // painting tools panel
        gbc.gridx = 1;
        this.getContentPane().add(paintingTool, gbc);

        // color dialog panel
        gbc.gridx = 2;
        this.getContentPane().add(colorDialog, gbc);

        // canvas panel
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 4;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        this.getContentPane().add(pnlBG, gbc);

        // draw all onto the screen
        this.pack();
        this.repaint();
        this.setVisible(true);
    }

    /**
     * get Image Icon from chosen path
     * @param path Path to get image
     * @return Image
     */
    public Image getImageIcon(String path){
        Image image = Toolkit.getDefaultToolkit().getImage(getClass().getResource(path));
        return image;
    }

    /**
     * run GUI asynchronously
     */
    @Override
    public void run() {
        createGUI();
    }

    public static void main(String[] args) {
        JFrame.setDefaultLookAndFeelDecorated(true);
        SwingUtilities.invokeLater(new MainFrame());
    }
}
