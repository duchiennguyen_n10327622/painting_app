package vecFrame;

import property.ColorDialog;
import vecShape.*;
import vecShape.Polygon;
import vecShape.Rectangle;
import vecFrame.PaintingTool.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.*;
import java.util.ArrayList;

/**
 * the Painting Canvas covering all the state and actions
 */
public class PaintingPad extends JPanel implements MouseListener, MouseMotionListener {
    // Painting pad properties and graphics buffer
    private int width = 0;
    private int height = 0;
    private BufferedImage buff_img, org_img;
    private Point start, end;
    private Graphics2D g2d, g2;

    // Painting pad tools and functions
    private PaintingTool paintingTool;
    private ColorDialog colorDialog;
    private Line line;
    private Plot plot;
    private Rectangle rectangle;
    private Polygon polygon;
    private Boolean drag;
    private Picker picker;
    private Ellipse ellipse;

    // vecfiles and redo status
    private String vecFile;
    private ArrayList<String> redoString = new ArrayList<>();

    /**
     * create the painting canvas
     * @param width width of the canvas
     * @param height height of the canvas
     */
    public PaintingPad(int width, int height) {
        initComponents();
        this.width = width;
        this.height = height;
        this.setSize(new Dimension(width, height));

        // double buffer graphics 2d
        org_img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        g2d = (Graphics2D)org_img.getGraphics();
        g2d.setColor(new Color(255, 255, 255));
        g2d.fillRect(0, 0, width, height);
        g2d.dispose();

        buff_img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        g2 = (Graphics2D) buff_img.getGraphics();
        g2.setColor(new Color(255, 255, 255));
        g2.fillRect(0, 0, width, height);
        g2.dispose();

        // Vec file
        vecFile = new String();

        // add event listener
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
    }

    /**
     * setup layouts
     */
    private void initComponents() {
        GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 300, Short.MAX_VALUE)
        );
    }

    /**
     *
     * @return vecFile
     */
    public String getVecFile() {
        return this.vecFile;
    }

    /**
     * get color from hex code
     * @param colorStr hexstring color
     * @return color
     */
    public static Color getColor(String colorStr) {
        return new Color(
                Integer.valueOf( colorStr.substring( 1, 3 ), 16 ),
                Integer.valueOf( colorStr.substring( 3, 5 ), 16 ),
                Integer.valueOf( colorStr.substring( 5, 7 ), 16 ) );
    }

    /**
     * draw approriate shape from given line of string
     * @param line line of string
     */
    public void drawString(String line){
        // split the line into arrays
        String[] tempArray = line.split(" ");
        Color refillColor = null;

        /*if the line start with pen or fill, there is no need to add the line to the vecFile
        * since it is handled by the colordialog's propertyChangeListener and it will automatically
        * write the pen color and fill color status into the vecfile.
        * if fill is off and another fill color is chosen, do the same fill color the second time because,
        * according to the propertyChangedListener of fill state it will automatically draw current fill color to
        * vecFile not the new fillcolor*/
        if (line.startsWith("PEN") || line.startsWith("FILL")) {
            if (line.startsWith("PEN")) {
                colorDialog.setPenColor(getColor(tempArray[1]));
            } else {
                if (line.endsWith("OFF")) {
                    colorDialog.setFillIsOn(false);
                } else {
                    if (!colorDialog.getFillIsOn()) {
                        colorDialog.setFillIsOn(true);
                        refillColor = getColor(tempArray[1]);
                    } else {
                        colorDialog.setFillColor(getColor(tempArray[1]));
                    }
                }
            }
        } else {
			// append the line to the vecFile
            vecFile += line + "\n";
			
			// list of points to store the points in the specified string
            ArrayList<Point> points = new ArrayList<>();
			
			// iterate through the array to store x and y coordinates into an arraylist
            for (int i = 1; i < tempArray.length; i += 2) {
                int xValue = (int) (Double.valueOf(tempArray[i]) * getWidth());
                int yValue = (int) (Double.valueOf(tempArray[i + 1]) * getWidth());
                points.add(new Point(xValue, yValue));
            }
			
			// draw approriate shape based on the string
            if (line.startsWith(DrawMode.LINE.toString())) {
                this.line = new Line();
                this.line.setPenColor(colorDialog.getPenColor());
                this.line.setPoint(points.get(0), points.get(1));
                this.line.draw(g2d);
                this.line.draw(g2);
            } else if (line.startsWith(DrawMode.PLOT.toString())) {
                plot = new Plot();
                plot.setPenColor(colorDialog.getPenColor());
                plot.setPoint(points.get(0), points.get(0));
                plot.draw(g2d);
            } else if (line.startsWith(DrawMode.RECTANGLE.toString())) {
                rectangle = new Rectangle();
                rectangle.setPenColor(colorDialog.getPenColor());
                rectangle.setFillColor(colorDialog.getFillColor());
                rectangle.setFillIsOn(colorDialog.getFillIsOn());
                rectangle.setPoint(points.get(0), points.get(1));
                rectangle.draw(g2d);
            } else if (line.startsWith(DrawMode.ELLIPSE.toString())) {
                ellipse = new Ellipse();
                ellipse.setPenColor(colorDialog.getPenColor());
                ellipse.setFillColor(colorDialog.getFillColor());
                ellipse.setFillIsOn(colorDialog.getFillIsOn());
                ellipse.setPoint(points.get(0), points.get(1));
                ellipse.draw(g2d);
            } else if (line.startsWith(DrawMode.POLYGON.toString())) {
                polygon = new Polygon();
                polygon.setPenColor(colorDialog.getPenColor());
                polygon.setFillColor(colorDialog.getFillColor());
                polygon.setFillIsOn(colorDialog.getFillIsOn());
				// add all points into the points array to draw polygon
                points.forEach(p -> polygon.addPoints(p));
                polygon.draw(g2d);
            }
        }
        // run recursive the same line if needs to refill
        if (refillColor != null) {
            // remove one line if current fill color is same as new fill color
            if (refillColor == colorDialog.getFillColor()) {
                String[] temp = vecFile.split("\n");
                vecFile = new String();
                for (int i = 0; i < temp.length - 1; i++) {
                    vecFile += temp[i] + "\n";
                }
            }
            drawString(line);
        }
    }
	
	/**
     * redo when redo button clicked
     */
    public void redo() {
        if (redoString.size() != 0) {
            drawString(redoString.get(redoString.size() - 1));
            redoString.remove(redoString.size() - 1);
            repaint();
        }
    }

	/**
     * undo when button undo clicked or ctrl+Z is observed
     */
    public void undo() {
        if (!vecFile.isEmpty()) {
            String[] lines = vecFile.split("\n");
            g2d.setColor(Color.WHITE);
            g2d.fillRect(0, 0, width, height);
            colorDialog.setPenColor(Color.BLACK);
            colorDialog.setFillColor(Color.WHITE);
            colorDialog.setFillIsOn(true);
            vecFile = new String();
            for (int i = 0; i < lines.length - 1; i++) {
                drawString(lines[i]);
            }
            repaint();

            redoString.add(lines[lines.length - 1]);
        }
    }

	/**
     * load file and draw onto current canvas
     * @param loadFile file to load
     */
    public void load(File loadFile) {
        try {
			// read from the file
            FileReader fr = new FileReader(loadFile);
            BufferedReader br = new BufferedReader(fr);
            String line;
			
			// empty the redo state
            redoString = new ArrayList<>();
			
			// erase all canvas
            g2d.setColor(Color.WHITE);
            g2d.fillRect(0, 0, width, height);
			
			//reset default fill and pen color state
            colorDialog.setPenColor(Color.BLACK);
            colorDialog.setFillColor(Color.WHITE);
            colorDialog.setFillIsOn(true);
			
			// empty vecFile
            vecFile = new String();
			
			// read each line and draw
            while ((line = br.readLine()) != null) {
                drawString(line);
            }
			
			// repaint canvas
            repaint();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

	/**
     * set paintingTool to be used
     * @param paintingTool painting tool to be used
     */
    public void setPaintingTool (PaintingTool paintingTool) {
        this.paintingTool = paintingTool;
		// assign propertyChangeListener to this paintingTool so that it will complete the polygon after tool changed
        this.paintingTool.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getOldValue() == DrawMode.POLYGON) {
                    drawPolygon();
                }

                if (evt.getNewValue() == DrawMode.POLYGON) {
                    polygon = new Polygon();
                    polygon.setPenColor(colorDialog.getPenColor());
                    polygon.setFillColor(colorDialog.getFillColor());
                    polygon.setFillIsOn(colorDialog.getFillIsOn());
                }
            }
        });
    }

	/**
     * set color dialog to be used
     * @param colorDialog color dialog to be used
     */
    public void setColorDialog (ColorDialog colorDialog) {
        this.colorDialog = colorDialog;
		// assign propertyChangeListener to this colorDialog
        this.colorDialog.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
				// perform if there is a change in properties
                if (evt.getOldValue() != null) {
					// if any properties of colorDialog changed while drawing polygon,
					// finish that polygon and create a new polygon
                    if (paintingTool.getDrawMode() == DrawMode.POLYGON) {
                        drawPolygon();
                        polygon = new Polygon();
                        polygon.setPenColor(colorDialog.getPenColor());
                        polygon.setFillColor(colorDialog.getFillColor());
                        polygon.setFillIsOn(colorDialog.getFillIsOn());
                        repaint();
                    }
					
					/*if pen color is changed
					*   update vecfile
					* if fill color is changed
					*   check if the fill color is on or off
					*       if on: update vecfile with new color
					*       if off: turn on the fill color, that event will be handle by fillsate property changed
					* if fill stated is changed
					*   if fill is turned off
					*       update vecfile with "OFF" state
					*   if fill is turned on
					*       update vecfile with current fill color of fill*/
                    if (evt.getPropertyName() == "Pen color changed") {
                        vecFile += "PEN #" + Integer.toHexString(((Color)evt.getNewValue()).getRGB()).substring(2).toUpperCase() + "\n";
                    } else if (evt.getPropertyName() == "Fill color changed") {
                        if (!colorDialog.getFillIsOn()) {
                            colorDialog.setFillIsOn(true);
                        } else {
                            vecFile += "FILL #" + Integer.toHexString(((Color) evt.getNewValue()).getRGB()).substring(2).toUpperCase() + "\n";
                        }
                    } else if (evt.getPropertyName() == "Fillstate changed") {
                        if ((Boolean)evt.getNewValue() == false) {
                            vecFile += "FILL OFF\n";
                        } else {
                            vecFile += "FILL #" + Integer.toHexString((colorDialog.getFillColor()).getRGB()).substring(2).toUpperCase() + "\n";
                        }
                    }
                }
            }
        });
    }

    /**
     * draw the polygon onto canvas
     */
    public void drawPolygon() {
        // draw polygon onto canvas
        polygon.draw(g2d);

        // save coordinates onto a string to pass into vecFile
        String coordinates = new String();
        //iterate through all of the points of polygon and save into coordinates
        for (Point point : polygon.getPoints()) {
            coordinates += String.format("%f %f ", (double)point.x/getSize().width,
                    (double)point.y/getSize().height);
        }

        // update vecFile
        vecFile += "POLYGON " + coordinates + "\n";

        // repaint
        repaint();
    }

    /**
     * Paint org_img to the screen
     */
    public void refresh() {
        g2d.drawImage(org_img, 0, 0, this);
        repaint();
    }

    /**
     * flush everything
     */
    public void flush() {
        // clear point
        start = null;
        end = null;
        polygon = null;

        // clean memory
        org_img.flush();
        buff_img.flush();
        System.gc();

        // clear image buffer
        org_img = null;
        buff_img = null;
        org_img = new BufferedImage(getSize().width, getSize().height, BufferedImage.TYPE_INT_RGB);
        g2 = (Graphics2D) org_img.getGraphics();
        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0, getSize().width, getSize().height);
        g2.dispose();
        refresh();
    }

    /**
     * paint the canvas as things got updated
     * @param g graphics
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g2 = (Graphics2D) g;
        if (buff_img == null) {
            buff_img = (BufferedImage) createImage(getSize().width, getSize().height);
            g2d = (Graphics2D) buff_img.getGraphics();
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            refresh();
        }

        // render image from buff_img to screen
        g2.drawImage(buff_img, null, 0, 0);

        // draw image to org_buff and show process on screen
        if (start != null && end != null) {
            switch (paintingTool.getDrawMode()) {
                case PLOT:
                    plot.draw(g2);
                    break;
                case LINE:
                    line.draw(g2);
                    break;
                case RECTANGLE:
                    rectangle.draw(g2);
                    break;
                case ELLIPSE:
                    ellipse.draw(g2);
                    break;
            }
        };
    }


    @Override
    public void mouseClicked(MouseEvent e) {
    }

    /**
     * get start point when mouse pressed and update the approriate chosen shape
     * @param e mouse pressed
     */
    @Override
    public void mousePressed(MouseEvent e) {
        // if mouse pressed remove everything in redoString
        if (!redoString.isEmpty()) {
            redoString = new ArrayList<>();
        }

        start = e.getPoint();

        switch (paintingTool.getDrawMode()) {
            case PLOT:
                plot = new Plot();
                plot.setPenColor(colorDialog.getPenColor());
                plot.setPoint(start, start);
                break;
            case LINE:
                line = new Line();
                line.setPenColor(colorDialog.getPenColor());
                line.setPoint(start, start);
                break;
            case RECTANGLE:
                rectangle = new Rectangle();
                rectangle.setPenColor(colorDialog.getPenColor());
                rectangle.setFillColor(colorDialog.getFillColor());
                rectangle.setFillIsOn(colorDialog.getFillIsOn());
                rectangle.setPoint(start, start);
                break;
            case ELLIPSE:
                ellipse = new Ellipse();
                ellipse.setPenColor(colorDialog.getPenColor());
                ellipse.setFillColor(colorDialog.getFillColor());
                ellipse.setFillIsOn(colorDialog.getFillIsOn());
                ellipse.setPoint(start, start);
                break;
            case POLYGON:
                // if set drag to be false and add points to polygon's point lists
                drag = false;
                polygon.addPoints(start);
        }
    }

    /**
     * get end point when mouse released and do approriate actions based on which tool is chosen
     * @param e mouse released
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        end = e.getPoint();

        // write to vecFile and draw shape to the canvas
        switch (paintingTool.getDrawMode()) {
            case PLOT:
                plot.draw(g2d);
                vecFile += String.format("PLOT %f %f\n", (double)start.x/getWidth(), (double)start.x/getHeight());
                break;
            case LINE:
                line.draw(g2d);
                vecFile += String.format("LINE %f %f %f %f\n", (double)start.x/getWidth(),
                        (double)start.y/getHeight(),
                        (double)end.x/getWidth(), (double)end.y/getHeight());
                break;
            case RECTANGLE:
                rectangle.draw(g2d);
                vecFile += String.format("RECTANGLE %f %f %f %f\n", (double)start.x/getWidth(),
                        (double)start.y/getHeight(),
                        (double)end.x/getWidth(), (double)end.y/getHeight());
                break;
            case ELLIPSE:
                ellipse.draw(g2d);
                vecFile += String.format("ELLIPSE %f %f %f %f\n", (double)start.x/getWidth(),
                        (double)start.y/getHeight(),
                        (double)end.x/getWidth(), (double)end.y/getHeight());
                break;
            case PICKER:
                picker = new Picker();
                picker.setColor(new Color(buff_img.getRGB(start.x, start.y)));
                colorDialog.setColor(picker.getColor());
                break;
            case POLYGON:
                /*if mouse is drag while drawing polygon
                *   delete initial drag point and add the end point of the drag to the polygon*/
                if (drag) {
                    polygon.getPoints().remove(polygon.getPoints().size() - 1);
                    polygon.addPoints(end);
                }

                // draw line as each of point is adding to polygon to show the transition
                if (polygon.getPoints().size() > 1) {
                    int lastIndex = polygon.getPoints().size() - 1;
                    int secondLastIndex = polygon.getPoints().size() - 2;
                    Point lastPoint = polygon.getPoints().get(lastIndex);
                    Point secondLastPoint = polygon.getPoints().get(secondLastIndex);
                    line = new Line();
                    line.setPenColor(colorDialog.getPenColor());
                    line.setPoint(secondLastPoint, lastPoint);
                    line.draw(g2d);
                }
        }

        // reset the start and end point
        start = null;
        end = null;
        repaint();
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    /**
     * do approriate actions for each shape when mouse dragged
     * @param e mouse dragged
     */
    @Override
    public void mouseDragged(MouseEvent e) {
        end = e.getPoint();

        switch (paintingTool.getDrawMode()) {
            case LINE:
                line.setPoint(start, end);
                break;
            case RECTANGLE:
                rectangle.setPoint(start, end);
                break;
            case ELLIPSE:
                ellipse.setPoint(start, end);
                break;
            case POLYGON:
                // set drag to be true
                drag = true;
        }

        repaint();
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }
}
