package property;

import javax.swing.*;
import java.awt.*;

/**
 * The color icon for each of the color cell in the color panel
 */
public class ColorCell extends ImageIcon{
    public static final int WIDTH = 25;
    public static final int HEIGHT = 25;
    private Point location;
    private Color color = Color.WHITE;
    public ColorCell(Color color){
        this.color = color;
    }

    /**
     * set color for the cell
     * @param color new cell color
     */
    public void setColor(Color color){
        this.color = color;
    }

    /**
     *
     * @return the cell color
     */
    public Color getColor(){
        return color;
    }

    /**
     * draw the color icon for current color cell
     * @param c current component
     * @param g graphics
     * @param x left coordinate to draw
     * @param y top coordinate to draw
     */
    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
        g.setColor(color);
        g.fillRect(6, 6, WIDTH+8, HEIGHT+8);
    }
}
