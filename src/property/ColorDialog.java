package property;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * this Jpanel contains all the color fucntioning GUI including
 * Color Pane and Fill + Pen color status
 */
public class ColorDialog extends JPanel {

    // icon for pen and fill color button
    private ImageIcon penColorIcon;
    private ImageIcon fillColorIcon;

    // pen and fill color
    private Color penColor;
    private Color fillColor;

    // fill button
    private JToggleButton btnFillColor;
    private JLabel lblFill;

    // pen button
    private JToggleButton btnPenColor;
    private JLabel lbPenColor;

    // button for changing fill state
    private JButton btnFillToggle;
    private JLabel lbFillToggle;
    private Boolean fillIsOn = true;

    // button group for choosing between pen and fill color
    private ButtonGroup fillAndPen;
    private JButton btncolorChooser;
    private ColorPalette colorPalette;


    /**
     * Creates new form ColorDialog
     */
    public ColorDialog() {
        // initialize default state
        penColorIcon = new ColorCell(Color.BLACK);
        fillColorIcon = new ColorCell(Color.WHITE);
        penColor = Color.BLACK;
        fillColor = Color.WHITE;

        // initialize components
        initComponents();

        btnPenColor.setIcon(penColorIcon);
        btnFillColor.setIcon(fillColorIcon);

        // set icon for the color chooser button with the path in the source folder
        btncolorChooser.setIcon(new ImageIcon(getImageIcon("/icon/ColorChooser.png")));
    }

    /**
     * image to assign to buttons
     * @param path path of the image
     * @return the image in the path
     */
    public Image getImageIcon(String path) {
        Image image = Toolkit.getDefaultToolkit().getImage(getClass().getResource(path));
        return image;
    }

    /**
     * initialize components and setup layouts
     */
    private void initComponents() {
        btnPenColor = new JToggleButton();
        btnPenColor.setSelected(true);
        lbPenColor = new JLabel();
        lbPenColor.setText("    Pen");

        fillAndPen = new ButtonGroup();
        btnFillColor = new JToggleButton();
        lblFill = new JLabel();
        lblFill.setText("      Fill");


        btnFillToggle = new JButton();
        btnFillToggle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fillToggle();
            }
        });
        lbFillToggle = new JLabel(" FILL ON");

        btncolorChooser = new JButton();
        btncolorChooser.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                colorChooserActionPerformed(evt);
            }
        });

        colorPalette = new ColorPalette();
        colorPalette.setBackground(new Color(204, 204, 255));
        colorPalette.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent evt) {
                colorPaneMousePressed(evt);
            }
        });

        fillAndPen.add(btnPenColor);
        fillAndPen.add(btnFillColor);

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(btnPenColor, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbPenColor))
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(btnFillColor, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblFill))
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(btnFillToggle, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                        .addComponent(lbFillToggle))
                .addGap(6, 6, 6)
                .addComponent(colorPalette, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(btncolorChooser, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(btnPenColor, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(lbPenColor))
            .addGroup(layout.createSequentialGroup()
                .addComponent(btnFillColor, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(lblFill))
            .addGroup(layout.createSequentialGroup()
                    .addComponent(btnFillToggle, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                    .addGap(11, 11, 11)
                    .addComponent(lbFillToggle))
            .addComponent(colorPalette, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addComponent(btncolorChooser, GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE)
        );
    }

    /**
     * change approriate pen and fill color based on color being clicked in the color pane
     * @param evt
     */
    private void colorPaneMousePressed(java.awt.event.MouseEvent evt) {
        Color color = colorPalette.getColor();
        setColor(color);
    }

    /**
     * open JColorChooser dialog when clicked colorChooser button
     * @param evt
     */
    private void colorChooserActionPerformed(java.awt.event.ActionEvent evt) {
        Color color = JColorChooser.showDialog(this, "specify your color", Color.blue);
        colorPalette.setImage(color);
        setColor(color);
    }

    /**
     * get current pen color
     * @return pen color
     */
    public Color getPenColor() {
        return penColor;
    }

    /**
     * get current fill color
     * @return fill color
     */
    public Color getFillColor() {
        return fillColor;
    }

    /**
     * set color for fill and pen depends on which one is being chosen
     * @param newColor color to be set
     */
    public void setColor(Color newColor){
        if (btnPenColor.isSelected()){
            setPenColor(newColor);
        } else if (btnFillColor.isSelected()) {
            setFillColor(newColor);
        }
    }

    /**
     * set new pen Color
     * @param newColor color to be set for pen
     */
    public void setPenColor(Color newColor) {
        Color oldPenColor = penColor;
        colorPalette.setImage(newColor);
        penColor = newColor;
        penColorIcon = colorPalette.getImage();
        btnPenColor.setIcon(penColorIcon);
        this.firePropertyChange("Pen color changed", oldPenColor, newColor);
    }

    /**
     * set new fill color
     * @param newColor color to be set for fill
     */
    public void setFillColor(Color newColor) {
        Color oldFillColor = fillColor;
        colorPalette.setImage(newColor);
        fillColor = newColor;
        fillColorIcon = colorPalette.getImage();
        btnFillColor.setIcon(fillColorIcon);
        this.firePropertyChange("Fill color changed", oldFillColor, fillColor);
    }

    /**
     * get the current state of fill color
     * @return true if fill is on; false if fill is off
     */
    public Boolean getFillIsOn() {
        return  this.fillIsOn;
    }

    /**
     * toggle the fill state
     */
    public void fillToggle() {
        if (fillIsOn) {
            lbFillToggle.setText("FILL OFF");
        } else {
            lbFillToggle.setText(" FILL ON");
        }
        setFillIsOn(!fillIsOn);
    }

    /**
     * set fill state to a new value
     * @param newState value for fill state to be set
     */
    public void setFillIsOn(Boolean newState) {
        Boolean oldState = fillIsOn;
        fillIsOn = newState;
        if (fillIsOn) {
            lbFillToggle.setText(" FILL ON");
        } else {
            lbFillToggle.setText("FILL OFF");
        }
        this.firePropertyChange("Fillstate changed", oldState, fillIsOn);
    }
}
