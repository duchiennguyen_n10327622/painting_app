package property;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import javax.swing.ImageIcon;

/**
 * Color Palette Panel
 */
public class ColorPalette extends javax.swing.JPanel {
    // 3 rows and 10 colums of prefixed color
    private int numRow = 3;
    private int numCols = 10;
    private ColorCell[][] cells = new ColorCell[numRow][numCols];
    private Color[][] colors = new Color[numRow][numCols];
    private ColorCell image;
    private Graphics2D g2d;
    private Point location;

    /**
     * Creates new form ColorPane1
     */
    public ColorPalette() {
        setLayout(null);
        image = new ColorCell(Color.WHITE);
        colors[0][0] = new Color(0, 0, 0);
        colors[0][1] = new Color(255, 0, 204);
        colors[0][2] = new Color(204, 204, 255);
        colors[0][3] = new Color(255, 0, 0);
        colors[0][4] = new Color(204, 102, 0);
        colors[0][5] = new Color(255, 255, 0);
        colors[0][6] = new Color(0, 153, 0);
        colors[0][7] = new Color(0, 153, 255);
        colors[0][8] = new Color(0, 0, 255);
        colors[0][9] = new Color(153, 0, 153);
        colors[1][0] = Color.WHITE;
        colors[1][1] = new Color(255, 153, 102);
        colors[1][2] = new Color(255, 174, 201);
        colors[1][3] = new Color(255, 201, 14);
        colors[1][4] = new Color(229, 238, 176);
        colors[1][5] = new Color(155, 153, 200);
        colors[1][6] = new Color(214, 102, 202);
        colors[1][7] = new Color(153, 51, 0);
        colors[1][8] = new Color(112, 146, 190);
        colors[1][9] = new Color(200, 191, 231);
        colors[2][0] = new Color(50, 150, 150);
        colors[2][1] = new Color(100, 100, 100);
        colors[2][2] = new Color(150, 150, 150);
        colors[2][3] = new Color(155, 253, 0);
        colors[2][4] = new Color(150, 100, 75);
        colors[2][5] = new Color(100, 155, 75);
        colors[2][6] = new Color(230, 120, 103);
        colors[2][7] = new Color(153, 151, 0);
        colors[2][8] = new Color(134, 255, 190);
        colors[2][9] = new Color(20, 75, 100);
        this.setPreferredSize(new Dimension(251, 76));
        // Copy colors from color array to the color cell array
        for (int i = 0; i < numRow; i++) {
            for (int j = 0; j < numCols; j++) {
                cells[i][j] = new ColorCell(colors[i][j]);
            }
        }

        // add mouse listener to get the which color is chosen by the mouse
        this.addMouseListener(new MouseAdapter() {
            /**
             * set Image to be the color of the chosen color cell
             * @param e mouse pressed
             */
            public void mousePressed(MouseEvent e) {
                setImage(e.getX(), e.getY());
            }

            /**
             * if mouse exited the color palette set location to be null and repaint
             * @param e mouse exited
             */
            public void mouseExited(MouseEvent e) {
                location = null;
                repaint();
            }
        });

        // add mouse motion listener to highlight the color cell when mouse hover by
        this.addMouseMotionListener(new MouseMotionAdapter() {
            /**
             * get the current position of the mouse in the color palette and repaint
             * @param e
             */
            public void mouseMoved(MouseEvent e) {
                location = e.getPoint();
                repaint();
            }
        });
    }

    /**
     * return current image
     * @return image
     */
    public ImageIcon getImage() {
        return image;
    }

    /**
     * get color of current image
     * @return image color
     */
    public Color getColor() {
        return image.getColor();
    }

    /**
     * set image to be the color of current chosen color cell
     * @param mouseX x position of the mouse
     * @param mouseY y position of the mouse
     */
    public void setImage(int mouseX, int mouseY) {
        if (mouseX % ColorCell.WIDTH == 0 || mouseY % ColorCell.HEIGHT == 0) {
            return;
        }
        int row = mouseY / ColorCell.HEIGHT;
        int col = mouseX / ColorCell.WIDTH;
        if (col < 0 || col > numCols - 1 || row < 0 || row > numRow - 1) {
            return;
        }
        image = cells[row][col];

    }

    /**
     * set image to some given color
     * @param color Color to be set for image
     */
    public void setImage(Color color) {
        image = new ColorCell(color);
    }

    /**
     * draw blue nearly transparent color on where the mouse is hover
     * @param mouseX x coordinate of the mouse
     * @param mouseY y coordinate of the mouse
     */
    public void drawCell(int mouseX, int mouseY) {
        if (mouseX % ColorCell.WIDTH == 0 || mouseY % ColorCell.HEIGHT == 0) {
            return;
        }
        int row = mouseY / ColorCell.HEIGHT;
        int col = mouseX / ColorCell.WIDTH;
        if (col < 0 || col > numCols - 1 || row < 0 || row > numRow - 1) {
            return;
        }
        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.3f));
        g2d.setPaint(Color.BLUE);
        g2d.fillRect(col * ColorCell.WIDTH, row * ColorCell.HEIGHT, ColorCell.WIDTH, ColorCell.HEIGHT);

    }

    /**
     * draw the whole color pallete
     * @param g graphics
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g2d = (Graphics2D) g;
        if (image == null) {
            this.createImage(ColorCell.WIDTH, ColorCell.HEIGHT);
        }
        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
        for (int i = 0; i < numRow; i++) {
            for (int j = 0; j < numCols; j++) {
                g.setColor(colors[i][j]);
                g.fillRect(j * ColorCell.WIDTH + 1, i * ColorCell.HEIGHT - 1, ColorCell.WIDTH - 2, ColorCell.HEIGHT - 2);
            }
        }
        if (location != null) {
            drawCell(location.x, location.y);
        }
    }

}
